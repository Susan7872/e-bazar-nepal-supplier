import firebase from "firebase/app"
import "firebase/firestore"
import "firebase/auth"

var firebaseConfig = {
    apiKey: "AIzaSyC_1AHEp-xIiS8Q1s7y4el8HnB6Fkmw1Wg",
    authDomain: "ebazarnepal-supplier.firebaseapp.com",
    databaseURL: "https://ebazarnepal-supplier.firebaseio.com",
    projectId: "ebazarnepal-supplier",
    storageBucket: "ebazarnepal-supplier.appspot.com",
    messagingSenderId: "1024275897599",
    appId: "1:1024275897599:web:47d422672a1687b62c00ca"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase;