import { message } from "antd";

export const addSupplierAction = (supplier) => {
    return (dispatch, getState, { getFirebase }) => {
        //make async call to database
        const firestore = getFirebase().firestore();

        firestore.collection("supplier-info").add({
            ...supplier
        }).then(() => {
            message.success("Supplier added successfully.").then(() => {
                window.location.href = "/";
            });
            dispatch({
                type: "ADD_SUPPLIER_SUCCESS",
                supplier
            })
        }).catch(err => {
            message.success("Supplier failed to add.");
            dispatch({
                type: "ADD_SUPPLIER_ERROR",
                err
            });
        });
    };
};