const initState = {

}

const addSupplierReducer = (state = initState, action) => {
    switch (action.type) {
        case "ADD_Supplier_SUCCESS": {
            return state;
        }
        case "ADD_SUPPLIER_ERROR": {
            return state;
        }

        default: return state;

    }
}

export default addSupplierReducer;