import { firebaseReducer } from 'react-redux-firebase';
import { firestoreReducer } from "redux-firestore";
import { combineReducers } from "redux";
import addSupplierReducer from './supplierReducer';

const rootReducer = combineReducers({
    firebase: firebaseReducer,
    firestore: firestoreReducer,
    supplier: addSupplierReducer
})

export default rootReducer;