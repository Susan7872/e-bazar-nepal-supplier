import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import AddSupplier from './component/AddSupplier';


function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={AddSupplier} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
