import { Button, Col, Form, Input, Row, Layout } from "antd";
import React, { Component } from "react";
import 'antd/dist/antd.css';
import { addSupplierAction } from "../store/actions/supplierAction";
import { connect } from "react-redux";
import Spinner from "../common/Spinner";


const { Content, Header } = Layout;

class AddSupplier extends Component {

    constructor(props) {
        super(props);
        this.state = {
            buttonClick: false
        }
    }

    render() {

        const submitSupplier = (value) => {
            if (value.mail === undefined) {
                value.mail = "null";
            }
            this.props.addSupplier(value);
        }

        return (
            <Spinner spinning={this.state.buttonClick}>
                <Header style={{ background: "#092143", padding: "0 40px", height: "100px" }}>
                    <Row>
                        <Col xs={12} sm={12} md={12} lg={12} xl={12} xxl={12}>
                            <img onClick={() => window.location.href = "https://ebazarnepal.com/"} style={{ cursor: "pointer", height: 100 }} src="https://i.ibb.co/3kxx78g/output-onlinepngtools.png" alt="E-Bazar Nepal Logo" />
                        </Col>
                        <Col xs={12} sm={12} md={12} lg={12} xl={12} xxl={12}>
                            <div style={{ float: "right" }}>
                                {/* <Button style={{ color: "black", cursor: "pointer", fontWeight: "bold", background: "none", border: "none", marginTop: 32 }} >Login</Button> */}
                            </div>
                        </Col>
                    </Row>
                </Header>
                <Content
                    className="site-layout"
                    style={{
                        margin: "40px 50px 20px 50px",
                        minHeight: 400,
                        color: "#fff",
                    }}
                >
                    <div style={{ textAlign: "center" }}>
                        <h3><b>Note: Please fill the form to become supplier of E-Bazar Nepal.</b></h3>

                    </div>
                    <Form layout="vertical" onFinish={submitSupplier}>
                        <Row gutter={30}>
                            <Col xs={24} sm={24} md={24} lg={24} xl={24} xxl={24}>
                                <Form.Item
                                    hasFeedback
                                    label="Shop Name"
                                    name="shop_name"
                                    rules={[
                                        { required: true, message: "This field is required!!!" },
                                    ]}
                                >
                                    <Input placeholder="Enter Shop name" />
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={24} md={24} lg={24} xl={24} xxl={24}>
                                <Form.Item
                                    hasFeedback
                                    label="Owner Name"
                                    name="owner_name"
                                    rules={[
                                        { required: true, message: "This field is required!!!" },
                                    ]}
                                >
                                    <Input placeholder="Enter Owner name" />
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={24} md={24} lg={24} xl={24} xxl={24}>
                                <Form.Item
                                    hasFeedback
                                    label="Product Category"
                                    name="product_items"
                                    rules={[
                                        { required: true, message: "This field is required!!!" },
                                    ]}
                                >
                                    <Input placeholder="Enter Product Items" />
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={24} md={24} lg={24} xl={24} xxl={24}>
                                <Form.Item
                                    hasFeedback
                                    label="Shop Address"
                                    name="shop_address"
                                    rules={[
                                        { required: true, message: "This field is required!!!" },
                                    ]}
                                >
                                    <Input placeholder="Enter Shop Address" />
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={24} md={24} lg={24} xl={24} xxl={24}>
                                <Form.Item
                                    hasFeedback
                                    label="Contact Number"
                                    name="contact"
                                    rules={[
                                        { required: true, message: "This field is required!!!" },
                                    ]}
                                >
                                    <Input placeholder="Enter Contact Number" />
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={24} md={24} lg={24} xl={24} xxl={24}>
                                <Form.Item
                                    hasFeedback
                                    label="E-mail"
                                    name="mail"
                                    rules={[
                                        {
                                            type: "email",
                                            message: "The input is not valid mail!",
                                        },
                                    ]}
                                >
                                    <Input placeholder="Enter mail id" />
                                </Form.Item>
                            </Col>
                        </Row>
                        <Form.Item>
                            <Button
                                htmlType="submit"
                                type="primary"
                                style={{
                                    width: 80,
                                    margin: "5px 0 20px 0",
                                    fontWeight: "bold"
                                }}
                                size="middle"
                            >
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                </Content>
            </Spinner>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addSupplier: supplier => dispatch(addSupplierAction(supplier))
    }
}


export default connect(null, mapDispatchToProps)(AddSupplier);